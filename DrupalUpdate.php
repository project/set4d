<?php
/**
 * Drupal shell update script adapted from http://drupal.org/node/233091
 * Assumptions: Full bootstrap and user id = 1 logged in.
 */
class DrupalUpdate {
  private $out;
  private $err;
  var $errorCode = 0;
  
  function __construct($out = STDOUT, $err = STDERR) {
    $this->out = $out;
    $this->err = $err;
  }
  
  function getUpdates($modules) {    
    $module_updates = array();
    foreach ($modules as $module) {
      $updates = drupal_get_schema_versions($module);
      if ($updates !== FALSE) {
        $filter = create_function('$a', 'return $a > drupal_get_installed_schema_version("'. $module .'");');
        $updates = array_filter($updates, $filter);
        if (!empty($updates)) {
          $module_updates[$module] = drupal_map_assoc($updates);
        }
      }
    }
    return $module_updates;
  }
  
  function goOffline() {
    variable_set('site_offline', 1);
    if (isset($this->message)) {
      $oldmessage = variable_get('site_offline_message',false);
      if ($oldmessage) { $this->old_message = $oldmessage; }
      variable_set('site_offine_message', $message);
    }
  }
  
  function backOnline() {
    variable_del('site_offline');
    if (!empty($this->message)) {
      if (is_set($this->oldmessage)) {
        variable_set('site_offline_message', $this->oldmessage);
      } else {
        variable_del('site_offine_message');
      }
    }
  }
  
  function update($updates) {
    fprintf($this->out, "Updating ". sizeof($updates) ." modules\n");
    foreach ($updates as $module => $update_list) {
      $this->updateModule($module, $update_list);
    }
    // When no updates remain, clear the caches in case the data has been updated.
    if (!empty($updates)) {
      $this->clearCache();
    }
    
    return $this->errorCode;
  }
  
  function updateModule($module, $update_list) {
    fprintf($this->out, "Updating $module\n");
    foreach ($update_list as $number) {
      do {
        $finished = $this->updateModuleN($module, $number);
      } while (!$finished);
    }    
  }
  
  function clearCache() {
    fprintf($this->out, "Clearing cache\n");
    cache_clear_all('*', 'cache', TRUE);
    cache_clear_all('*', 'cache_page', TRUE);
    cache_clear_all('*', 'cache_menu', TRUE);
    cache_clear_all('*', 'cache_filter', TRUE);
    drupal_clear_css_cache();
  }

  function updateModuleN($module, $number) {
    $results = module_invoke($module, 'update_'. $number);
    $finished = ($results['#finished'] <= 1);
    unset($results['#finished']);    
    $this->printResultsAndSetErrorCode($results, $module, $number);    
    $finished = ($results['#finished'] <= 1);
    if ($finished) {
      // Update the installed version
      drupal_set_installed_schema_version($module, $number);
    }

    return $finished;
  }
  
  function printResultsAndSetErrorCode($results, $module, $number) {
    foreach ($results as $result) {
      if (!$result['success']) {
        fprintf($this->err, "Error invoking ". $module ."_update_". $number ."\n Results\n ". var_export($result, TRUE) ."\n\n");
        $this->errorCode = 1;
      } else {
        fprintf($this->out, "Complete ". $module ."_update_". $number ."\n Query\n ". $result['query'] ."\n\n");
      }
    }    
  }
}

