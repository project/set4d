<?php
require_once dirname(__FILE__) .'/../DrupalUserAgent.php';
require_once 'autorun.php';

class DrupalUserAgentTest extends UnitTestCase {
  
  /**
   * Requires internet connection to work
   */
  function testGet() {
    $ua = new DrupalUserAgent('http://example.com');
    $content = $ua->get('');
    $this->assertNotEqual(FALSE, $content);
    $this->assertPattern("/Example Web Page/", $content);
  }

  function testParamString() {
    $this->assertEqual("name=value", DrupalUserAgent::paramString(array('name' => 'value')));
    $this->assertEqual("x=y&a=b", DrupalUserAgent::paramString(array('x' => 'y', 'a' => 'b')));
    $this->assertEqual("has+space=has%26ampersand", DrupalUserAgent::paramString(array('has space' => 'has&ampersand')));
  }
}

class DrupalFormLoaderTest extends UnitTestCase {
  
  function testFindFormBySubmitId() {
    $dom = $this->dom("<form action=doit><input name='x' /><input type=submit value='clickme' /></form>");
    $this->assertNotNull(DrupalFormLoader::findBySubmitValue($dom, 'clickme'));
  }
  
  function testFindFormWhenMultiple() {
    $html = <<<EOF
<form action='one'>
  <input type=submit value='dontclickme'/>
</form>
<form action='two'>
  <input type=submit value='clickme' />
</form>
EOF;
    $this->assertNotNull(DrupalFormLoader::findBySubmitValue($this->dom($html), 'clickme'));    
  }
  
  function testCantFindForm() {
    $proc = new DrupalFormLoader(NULL);
    $this->assertNull(DrupalFormLoader::findBySubmitValue($this->dom('<html/>'), 'clickme'));    
    $this->assertNull(DrupalFormLoader::findBySubmitValue($this->dom('<form/>'), 'clickme'));    
    // no form
    $this->assertNull(DrupalFormLoader::findBySubmitValue($this->dom("<input type='submit' value='clickme'/>"), 'clickme'));    
  }
  
  function testFindFormById() {
    $dom = $this->dom("<form id='1'/><form action='bang' id='2'/><form id='3'/>", '2');
    $form = DrupalFormLoader::findById($dom, '2');
    $this->assertNotNull($form);
    $this->assertEqual('bang', $form->getAction());
  }

  function testGetAction() {
    $form = $this->form("<form action='doit' />");
    $proc = new DrupalFormLoader($form);
    $this->assertEqual('doit', $proc->getAction());
  }
  
  function testSetValue() {
    $form = $this->form(<<<EOF
<form>
   <input type=text name='x' value='joe'/>
 /form>
EOF
    );    
    $proc = new DrupalFormLoader($form);
    $proc->setValue('x', 'john');
    $this->assertEqual(array('x' => 'john'), $proc->getParams());    
  }
  
  function testGetParams() {
    $form = $this->form(<<<EOF
<form>
   <input type=text name='x' value='joe'/>
</form>
EOF
    );    
    $proc = new DrupalFormLoader($form);
    $this->assertEqual(array('x' => 'joe'), $proc->getParams());
  }
  
  function form($html_string) {
    $forms = $this->dom($html_string)->elements->xpath('//form');
    return $forms[0];
  }
  
  function dom($html_string) {
    $html = DOMDocument::loadHTML($html_string);
    $dom = simplexml_import_dom($html);
    return $dom;
  }  
  
  function testAbsoluteUrl() {
    $ua = new DrupalUserAgent("http://example.com/");
    $this->assertEqual('http://example.com/kuku', $ua->absoluteUrl("kuku"));
    $this->assertEqual('http://example.com/bongo', $ua->absoluteUrl("http://example.com/bongo"));
    $this->assertEqual('http://example.com/', $ua->absoluteUrl(NULL));
  }
}

?>