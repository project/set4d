<?php
require_once dirname(__FILE__) .'/../DrupalInstaller.php';
require_once('mock_objects.php');
require_once 'autorun.php';

class DrupalInstallerTest extends UnitTestCase {
  
  function testInstaller() {
    Mock::generate('DrupalUserAgent');
    $ua = new MockDrupalUserAgent();
    $ua->expectCallCount('get', 3);
    $ua->setReturnValueAt(0, 'get', "Remaining 1");
    $ua->setReturnValueAt(1, 'get', "Remaining 0");    
    $ua->expectOnce('getDom', array('install.php?profile=&locale=en&id=1'));
    $dom = $this->dom("<form action=doit id='install-configure-form'><input name='bongo' /><input type=submit value='clickme' /></form>");    
    $ua->setReturnValue('getDom', $dom);
            
    $ua->expectOnce('post', array('doit', array(
      'site_name' => 'n', 
      'site_mail' => 'm', 
      'account[name]' => 'u', 
      'account[mail]' => 'm',
      'account[pass][pass1]' => 'p', 
      'account[pass][pass2]' => 'p',
      'bongo' => 'kuku')));
    $ua->setReturnValue('post', 'complete');
    $installer = new DrupalInstaller('some-protocol://some-website');
    $installer->site_name = 'n';
    $installer->email = 'm';
    $installer->username = 'u';
    $installer->password = 'p';
    $installer->additionalParams = array('bongo' => 'kuku');
    $installer->run(STDOUT, $ua);
  }
  
  function dom($html_string) {
    $html = DOMDocument::loadHTML($html_string);
    $dom = simplexml_import_dom($html);
    return $dom;
  }

  function test_array_remove() {
    $ary = array('a' => 1, 'b' => 2);
    $this->assertEqual(1, DrupalInstaller::array_remove($ary, 'a'));
    $this->assertEqual(array('b' => 2), $ary);
    $this->assertEqual(NULL, DrupalInstaller::array_remove($ary, 'not-there'));
  }
}

?>