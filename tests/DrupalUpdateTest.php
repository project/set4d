<?php
require_once dirname(__FILE__) .'/../DrupalUpdate.php';
require_once 'PHPMockFunction.php';
require_once 'autorun.php';

class DrupalUpdateTest extends UnitTestCase {
  
  function testUpdateModule() {
    $module_invoke = PHPMockFunction::mock("module_invoke");
    
    $returnValues = array( 
      array('#finished' => 3, array('success' => True)),
      array('#finished' => 2, array('success' => True)),
      array('#finished' => 1, array('success' => True)));
    
    $module_invoke->expects(InvocationRestriction::noMoreThan(3))
      ->with('bird-watcher', 'update_1')
      ->will(WillAction::returnValueStack($returnValues));
      
    $set_installed = PHPMockFunction::mock("drupal_set_installed_schema_version");
    $set_installed->expects(InvocationRestriction::once())->with('bird-watcher', 1);
    
    $up = new DrupalUpdate();    
    $up->updateModule('bird-watcher', array(1));
    $this->assertEqual(0, $up->errorCode);    
  }
  
  function testGetUpdates() {
    $schema_versions = PHPMockFunction::mock("drupal_get_schema_versions");
    $schema_versions->expects(InvocationRestriction::once())
      ->with('module1')
      ->will(WillAction::returnValue(array(1,2,3)));
    
    $installed_versions = PHPMockFunction::mock("drupal_get_installed_schema_version");
    $installed_versions->expects(InvocationRestriction::noMoreThan(3))
      ->with('module1')
      ->will(WillAction::returnValue(1));
    
    $map_assoc = PHPMockFunction::mock("drupal_map_assoc");
    $map_assoc->expects(InvocationRestriction::once())
      ->with(array(1 => 2,2 => 3)) // indexes starting at 1 not relevant
      ->will(new WillCallback(create_function('$versions', 'return join($versions, ",");')));      
    
    $up = new DrupalUpdate();
    $actual = $up->getUpdates(array("module1"));
    $expected = array("module1" => "2,3");
    $this->assertEqual($expected, $actual);
  }
  
  function testSuccessfulPrintResultsAndSetErrorCode() {
    $out = fopen("php://memory", "w");
    $err = fopen("php://memory", "w");
    $up = new DrupalUpdate($out, $err);    
    $results = array(
      array('success' => True, 'query' => 'successful sql here'), 
      array('success' => False, 'query' => 'unsuccessful sql here'));
    $up->printResultsAndSetErrorCode($results, 'bird-watcher', 1);   

    rewind($out);
    $expected = <<<MSG
Complete bird-watcher_update_1
 Query
 successful sql here


MSG;
    $this->assertEqual($expected, stream_get_contents($out));    
    
    rewind($err);
    $expected = <<<MSG
Error invoking bird-watcher_update_1
 Results
 array (
  'success' => false,
  'query' => 'unsuccessful sql here',
)


MSG;
    $this->assertEqual($expected, stream_get_contents($err));    
  }
  
  function testFailedUpdate() {
    $module_invoke = PHPMockFunction::mock("module_invoke");
    
    $returnValues = array( 
      array(array('success' => False, 'query' => 'update sql here')),
      array('#finished' => 1));
    
    $module_invoke->expects(InvocationRestriction::noMoreThan(2))
      ->with('bird-watcher', 'update_1')
      ->will(WillAction::returnValueStack($returnValues));
      
    $set_installed = PHPMockFunction::mock("drupal_set_installed_schema_version");
    $set_installed->expects(InvocationRestriction::once())->with('bird-watcher', 1);
    
    $devnull = fopen("php://memory", "w");
    $up = new DrupalUpdate($devnull, $devnull);    
    $results = $up->updateModule('bird-watcher', array(1));
    $this->assertEqual(1, $up->errorCode);    
  }
}


