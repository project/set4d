<?php

require_once dirname(__FILE__) .'/../DrupalCli.php';
require_once 'autorun.php';

class DrupalCliTest extends UnitTestCase {
  
  function testSetup() {
    $env = array();

    $env['argv'] = array('script_name');
    DrupalCli::Setup($env);
    $host = (array_key_exists('HTTP_HOST', $env) ? $env['HTTP_HOST'] : '');
    $this->assertEqual('localhost', $host);
   
    $env['argv'] = array('script_name', '--url=http://example.org');
    DrupalCli::Setup($env);
    $host = (array_key_exists('HTTP_HOST', $env) ? $env['HTTP_HOST'] : '');
    $this->assertEqual('example.org', $host);
  }
  
  function testParseArgumentsMultiValue() {   
    $actual = DrupalCli::ParseArguments(array('scriptname', '--a=b', '--a=c'), True);
    $expected = array('extraArguments' => array(), 'a' => 'c', 'a[]' => array('b', 'c'));
    $this->assertEqual($expected, $actual);
  }
  
  function testParseArguments() {
    $actual = DrupalCli::ParseArguments(array('scriptname', '--a=b', '-c', 'xxx'));
    $expected = array('a' => 'b', 'c' => '', 'extraArguments' => array('xxx'));
    $this->assertEqual($expected, $actual);
  }
  
  function testParseDoubleDashNoArguments() {
    $actual = DrupalCli::ParseArguments(array('scriptname', '--aa'));
    $this->assertTrue(isset($actual['aa']));
  }
  
  function testParseHyphenedExtraArguments() {
    $actual = DrupalCli::ParseArguments(array('scriptname', '--alpha-beta=b', 'gamma-lambda'));
    $expected = array('alpha-beta' => 'b', 'extraArguments' => array('gamma-lambda'));
    $this->assertEqual($expected, $actual);
  }
  
  function testAddItemAsArray() {
    $actual = array();
    DrupalCli::addItemAsArray(&$actual, 'bird', 'duck');
    $this->assertEqual(array('bird[]' => array('duck')), $actual);
    
    DrupalCli::addItemAsArray(&$actual, 'bird', 'pigeon');
    $this->assertEqual(array('bird[]' => array('duck', 'pigeon')), $actual);
  }
}

?>