#!/usr/bin/php
<?php

require_once dirname(__FILE__) .'/DrupalUserAgent.php';

/**
 * Installs Drupal by screen scraping installer pages.  Only tests on D6 and D7
 */
class DrupalInstaller {
  public $additionalParams = array();
  public $site_name;
  public $email;
  public $username;
  public $password;
  public $profile;
  public $db;
  public $db_user;
  public $db_password;
  
  public function install($baseurl) {
    $userAgent = new DrupalUserAgent($baseurl);
    $this->run(STDOUT, $userAgent);
  }
  
  public function run($stdout, DrupalUserAgent $userAgent) {
    $i = 1;    
    $done = False;

    // hit landing page first
    $userAgent->get("install.php");    
    $i = 0;
    while (!$done) {
      if ($i++ > 10) {
        echo $html;
        throw new Exception("Maximum number of calls made to $url. Site may not be setup correctly.");
      }

      // Implies english locale
      $html = $userAgent->get("install.php?profile=". $this->profile ."&locale=en&id=1&op=do_nojs");
      echo $stdout, "Step $i...\n";
      if (preg_match('/Database configuration/', $html)) {
        $dom = DrupalUserAgent::asDom($html);
        $this->configureDatabase($stdout, $dom, $userAgent);   
      } else {
        $done = preg_match('/(Remaining 0|100%)/', $html);
      }
    }
    
    echo $stdout, "Step ". $i ." ...";
    $dom = $userAgent->get("install.php?profile=". $this->profile ."&locale=en&id=1&op=finished");
    
    echo $stdout, "configuring ...";
    $dom = $userAgent->getDom("install.php?profile=". $this->profile ."&locale=en&id=1");
    $form = DrupalFormLoader::findById($dom, "install-configure-form");
    if ($form == NULL) {
      echo $dom->asXML();
      throw new Exception("Could not find installation form in installation screen");
    }
    $form->setValue('site_name', $this->site_name);
    $form->setValue('site_mail', $this->email);
    $form->setValue('account[name]', $this->username);
    $form->setValue('account[mail]', $this->email);
    $form->setValue('account[pass][pass1]', $this->password);
    $form->setValue('account[pass][pass2]', $this->password);
    $fields = array_merge($form->getParams(), $this->additionalParams);
    $html = $userAgent->post($form->getAction(), $fields);
    if (!preg_match('/complete/', $html)) {
      echo $html;
      throw new Exception('Configuration not complete');
    }
    echo $stdout, "finished.";
  }
  
  function configureDatabase($stdout, $dom, DrupalUserAgent $userAgent) {
    $formId = "install-settings-form";
    $form = DrupalFormLoader::findById($dom, $formId);
    if ($form == NULL) {
      echo $dom->asXML();
      throw new Exception("Could not find installation '$formId' form in installation screen");
    }
    $form->setValue('database', $this->db);
    $form->setValue('username', $this->db_user);
    $form->setValue('password', $this->db_password);
    $userAgent->post($form->getAction(), $form->getParams());    
  }

  static function array_remove(&$array, $item) {
    $value = $array[$item];
    unset($array[$item]);
    return $value;
  }
}


