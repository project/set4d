<?php  
  /////////////////////////////////////////////////////////////////////////////
  // Verbatim copies of functions from update.php that cannot loaded because update.php
  // has loose executable code and therefore wasn't written to be reused. 
  
  /**
   * @file
   * Administrative page for handling updates from one Drupal version to another.
   *
   * Point your browser to "http://www.example.com/update.php" and follow the
   * instructions.
   *
   * If you are not logged in as administrator, you will need to modify the access
   * check statement below. Change the TRUE to a FALSE to disable the access
   * check. After finishing the upgrade, be sure to open this file and change the
   * FALSE back to a TRUE!
   */
  
  function update_sql($sql) {
    $result = db_query($sql);
    return array('success' => $result !== FALSE, 'query' => check_plain($sql));
  }
  
  /**
   * Add a column to a database using syntax appropriate for PostgreSQL.
   * Save result of SQL commands in $ret array.
   *
   * Note: when you add a column with NOT NULL and you are not sure if there are
   * already rows in the table, you MUST also add DEFAULT. Otherwise PostgreSQL
   * won't work when the table is not empty, and db_add_column() will fail.
   * To have an empty string as the default, you must use: 'default' => "''"
   * in the $attributes array. If NOT NULL and DEFAULT are set the PostgreSQL
   * version will set values of the added column in old rows to the
   * DEFAULT value.
   *
   * @param $ret
   *   Array to which results will be added.
   * @param $table
   *   Name of the table, without {}
   * @param $column
   *   Name of the column
   * @param $type
   *   Type of column
   * @param $attributes
   *   Additional optional attributes. Recognized attributes:
   *     not null => TRUE|FALSE
   *     default  => NULL|FALSE|value (the value must be enclosed in '' marks)
   * @return
   *   nothing, but modifies $ret parameter.
   */
  function db_add_column(&$ret, $table, $column, $type, $attributes = array()) {
    if (array_key_exists('not null', $attributes) and $attributes['not null']) {
      $not_null = 'NOT NULL';
    }
    if (array_key_exists('default', $attributes)) {
      if (is_null($attributes['default'])) {
        $default_val = 'NULL';
        $default = 'default NULL';
      }
      elseif ($attributes['default'] === FALSE) {
        $default = '';
      }
      else {
        $default_val = "$attributes[default]";
        $default = "default $attributes[default]";
      }
    }
  
    $ret[] = update_sql("ALTER TABLE {". $table ."} ADD $column $type");
    if ($default) { $ret[] = update_sql("ALTER TABLE {". $table ."} ALTER $column SET $default"); }
    if ($not_null) {
      if ($default) { $ret[] = update_sql("UPDATE {". $table ."} SET $column = $default_val"); }
      $ret[] = update_sql("ALTER TABLE {". $table ."} ALTER $column SET NOT NULL");
    }
  }
  
  /**
   * Change a column definition using syntax appropriate for PostgreSQL.
   * Save result of SQL commands in $ret array.
   *
   * Remember that changing a column definition involves adding a new column
   * and dropping an old one. This means that any indices, primary keys and
   * sequences from serial-type columns are dropped and might need to be
   * recreated.
   *
   * @param $ret
   *   Array to which results will be added.
   * @param $table
   *   Name of the table, without {}
   * @param $column
   *   Name of the column to change
   * @param $column_new
   *   New name for the column (set to the same as $column if you don't want to change the name)
   * @param $type
   *   Type of column
   * @param $attributes
   *   Additional optional attributes. Recognized attributes:
   *     not null => TRUE|FALSE
   *     default  => NULL|FALSE|value (with or without '', it won't be added)
   * @return
   *   nothing, but modifies $ret parameter.
   */
  function db_change_column(&$ret, $table, $column, $column_new, $type, $attributes = array()) {
    if (array_key_exists('not null', $attributes) and $attributes['not null']) {
      $not_null = 'NOT NULL';
    }
    if (array_key_exists('default', $attributes)) {
      if (is_null($attributes['default'])) {
        $default_val = 'NULL';
        $default = 'default NULL';
      }
      elseif ($attributes['default'] === FALSE) {
        $default = '';
      }
      else {
        $default_val = "$attributes[default]";
        $default = "default $attributes[default]";
      }
    }
  
    $ret[] = update_sql("ALTER TABLE {". $table ."} RENAME $column TO ". $column ."_old");
    $ret[] = update_sql("ALTER TABLE {". $table ."} ADD $column_new $type");
    $ret[] = update_sql("UPDATE {". $table ."} SET $column_new = ". $column ."_old");
    if ($default) { $ret[] = update_sql("ALTER TABLE {". $table ."} ALTER $column_new SET $default"); }
    if ($not_null) { $ret[] = update_sql("ALTER TABLE {". $table ."} ALTER $column_new SET NOT NULL"); }
    $ret[] = update_sql("ALTER TABLE {". $table ."} DROP ". $column ."_old");
  }  
