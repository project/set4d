<?php
require_once(dirname(__FILE__) .'/TestRunner.php');

/**
 * Run tests for D6 or older. D6 still uses simpletest from simpletest.sourceforge.net
 */
class DrupalTestRunner extends TestRunner {

  function initialize($timeout = TestRunner::DEFAULT_TIMEOUT) {
    require_once './includes/bootstrap.inc';
    
    // do enough of a bootstrap to disable caching, otherwise an exit is
    // called, then finish bootstap with caching off 
    drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION);
    variable_set('cache', CACHE_DISABLED, False);
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    
    parent::initialize($timeout);
     
    // avoid notice exceptions
    // Show all errors, except for notices and coding standards warnings
    ini_set('error_reporting', E_ALL & ~E_NOTICE);
    simpletest_load();
  }
  
  function &getTests($files) {
    $tests = array();
    foreach ($files as $file) {
      $loader = new SimpleFileLoader();
      $suite = $loader->load($file);
      // extract tests from testsuite object and throw away
      // testsuite as it's not something simpletest module would 
      // recognize or need
      $tests = array_merge($tests, $suite->_test_cases);
    }
    return $tests;
  }
    
  function run($files) {
    $tests = $this->getTests($files);
    $results = simpletest_run_tests($tests, 'text');
    return ($results ? 0 : 1);
  }  
}
