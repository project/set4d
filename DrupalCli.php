<?php
/**
 * Helper for CLI driven scripts.
 */
class DrupalCli {
  
  /**
   * Read and setup _SERVER env for running drupal code from CLI
   * 
   * Example:
   *  DrupalCli::Setup(&$_SERVER)
   *
   * @param array $env $_SERVER
   */
  static public function Setup(&$env) {
    $host = 'localhost';
    $path = '';
    $args = DrupalCli::ParseArguments($env['argv']); 
    if (isset($args['url'])) {
      $parse = parse_url($args['url']);
      $host = $parse['host'];
      $path = (array_key_exists('path', $parse) ? $parse['path'] : '');
      if (isset($parse['port']) && is_numeric($parse['port'])) {
        $env['SERVER_PORT'] = $parse['port'];
        $host .= ':'.$parse['port'];
      }
    }
    
    # useful for debugger where you cannot control current directory
    if (isset($args['chdir'])) {
      chdir($args['chdir']);
    }
    
    $env['HTTP_HOST'] = $host;
    $env['REMOTE_ADDR'] = '127.0.0.1';
    $env['SERVER_ADDR'] = '127.0.0.1';
    $env['SERVER_SOFTWARE'] = 'PHP/curl';
    $env['SERVER_NAME'] = 'localhost';
    $env['REQUEST_URI'] = $path .'/';
    $env['REQUEST_METHOD'] = 'GET';
    $env['SCRIPT_NAME'] = $path .'/index.php';
    $env['PHP_SELF'] = $path .'/index.php';
    $env['HTTP_USER_AGENT'] = 'Drupal command line';
  }
  
  /**
   * Parses simple parameters from CLI. 
   * 
   * Puts trailing parameters into string array in 'extraArguments' 
   * 
   * Example:
   * $args = DrupalCli::ParseArguments($_SERVER['argv']);
   * if ($args['verbose']) echo "Verbose Mode On\n";
   * $files = $args['extraArguments'];
   *
   * Example CLI:
   *  --foo=blah -x -h  some trailing arguments 
   * 
   * if multiValueMode is true 
   * Example CLI:
   *  --include=a --include=b --exclude=c 
   * Then
   *  $args = DrupalCli::ParseArguments($_SERVER['argv']);
   *  $args['include[]'] will equal array('a', 'b')
   *  $args['exclude[]'] will equal array('c')
   *  $args['exclude'] will equal c
   *  $args['include'] will equal b   NOTE: only keeps last value
   * 
   * @param unknown_type $argv
   * @param supportMutliValue - will store 2nd copy of value in an array with key "foo[]" 
   * @return unknown
   */
  static public function ParseArguments($argv, $mutliValueMode = False) {
    $args = array();
    $args['extraArguments'] = array();
    array_shift($argv); // scriptname
    foreach ($argv as $arg) {
      if (ereg('^--([^=]+)=(.*)', $arg, $reg)) {
        $args[$reg[1]] = $reg[2];
        if ($mutliValueMode) {
          DrupalCli::addItemAsArray(&$args, $reg[1], $reg[2]);
        }
      } elseif (ereg('^[-]{1,2}([^[:blank:]]+)', $arg, $reg)) {
        $nonnull = '';
        $args[$reg[1]] = $nonnull;
        if ($mutliValueMode) {
          DrupalCli::addItemAsArray(&$args, $reg[1], $nonnull);
        }
      } else {
          $args['extraArguments'][] = $arg;
      }
    }
    
    return $args;
  }

  /**
   * Adds a value as an array of one, or appends to an existing array elements 
   *
   * @param unknown_type $array
   * @param unknown_type $item
   */
  static function addItemAsArray($array, $key, $item) {
      $array_key = $key .'[]';
      if (array_key_exists($array_key, $array)) {
        $array[$array_key][] = $item;               
      } else {
        $array[$array_key] = array($item);               
      }
  }
  
  static function AssertInDocRoot() {
    if (!file_exists('includes/bootstrap.inc')) {
      throw new Exception("Must be run from web docroot. Current working directory is:\n". getcwd());
    }
    define('DRUPAL_ROOT', getcwd());
  }
}

?>
