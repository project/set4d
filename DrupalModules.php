#!/usr/bin/php
<?php

/**
 * Drupal module enabler
 */
class DrupalModules {

  function __construct() {
    global $user;
    module_rebuild_cache();

    // probobaly ain't right, but ran into at least one module
    // that assumes this
    $user = user_load(array('uid' => 1));
  }

  function enable($name) {
    module_enable(array($name));
    drupal_install_modules(array($name));
  }

  function flush() {
    menu_rebuild();
    node_types_rebuild();
    drupal_clear_css_cache();
  }
}


