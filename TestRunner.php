<?php
require 'test_case.php';
require_once(dirname(__FILE__) .'/DrupalCli.php');

class TestRunner {
  
  const DEFAULT_TIMEOUT = 360;
  
  function initialize($timeout = TestRunner::DEFAULT_TIMEOUT) {
    
    // If not in 'safe mode', increase the maximum execution time:
    if (!ini_get('safe_mode')) {
      set_time_limit($timeout);
    }
  }

  function run($files) {
    $results;
    $reporter = new DefaultReporter();
    foreach ($files as $file) {
      $loader = new SimpleFileLoader();
      $suite = $loader->load($file);
      $result = $suite->run($reporter);
      if ($result) {
        $results = $result;
      }
    }
    
    return ($results ? 0 : 1);
  }  
}

?>